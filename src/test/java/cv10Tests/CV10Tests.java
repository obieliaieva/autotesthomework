package cv10Tests;

import cv10.page.BasePage;
import cv10.page.LogInPage;
import cv10.page.Projects;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CV10Tests extends BasePage {
    @Test
    public void searchForProject(){
        String projectName="RBI_015";
        Projects searchProject = new Projects();
        searchProject.logIn("obieliaieva","**");
        driver=searchProject.searchForProject(projectName);
        Assert.assertTrue(driver.findElement(By.xpath("//*[text()='RBI_015']")).isDisplayed());

    }
    @Test
    public void logIn(){
        LogInPage logInP=new LogInPage();
        driver=logInP.logIn("obieliaieva","**").logOut();
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='login-logo']")).isDisplayed());

    }
    @Test
    public void dbSource(){
        LogInPage db=new LogInPage();
        driver=db.logIn("obieliaieva","**").goToDBsource();
        Assert.assertTrue(driver.findElement(By.xpath("//body[@class='v-generated-body v-sa v-ch v-webkit v-win']")).isDisplayed());
    }

}
