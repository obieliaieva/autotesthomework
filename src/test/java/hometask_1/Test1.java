package hometask_1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test1 {
    @Test
    public void searchReport() {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");

        WebElement logName = driver.findElement(By.xpath("//*[@id='username']"));
        logName.sendKeys("username");

        WebElement logPassw = driver.findElement(By.xpath("//*[@id='password']"));
        logPassw.sendKeys("password");

        WebElement logIn = driver.findElement(By.xpath("//*[@class='login-submit']"));
        logIn.click();

        WebElement findAllBranches = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ALL_BRANCHES']")));
        findAllBranches.click();

        WebElement lookupProject = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@class,'split-pane-component-1')]//*[@id='ic-lookup-big']")));
        lookupProject.click();

        WebElement searchProject = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input")));
        searchProject.sendKeys("RBI_015");

        WebElement project = driver.findElement(By.xpath("//*[text()='RBI_015']"));
        String pr = project.getText();

        Assert.assertEquals(pr, "RBI_015");

        driver.close();


    }
}
