package hometask_1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test2 {
    @Test
    public void openSource() {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");

        WebElement logName = driver.findElement(By.xpath("//*[@id='username']"));
        logName.sendKeys("username");

        WebElement logPassw = driver.findElement(By.xpath("//*[@id='password']"));
        logPassw.sendKeys("password");

        WebElement logIn = driver.findElement(By.xpath("//*[@class='login-submit']"));
        logIn.click();

        WebElement menu = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gwt-uid-3']")));
        menu.click();

        WebElement sysSettings = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='System Settings']")));
        sysSettings.click();

        WebElement dbSource = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='DB Source']")));
        dbSource.click();

        WebElement forCheck = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='DB Sources']")));
        String flag = forCheck.getText();

        Assert.assertEquals(flag, "DB Sources");

        driver.close();


    }
}
