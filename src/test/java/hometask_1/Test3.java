package hometask_1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test3 {
    @Test
    public void logOut(){
        WebDriver driver =new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");

        WebElement logName=driver.findElement(By.xpath("//*[@id='username']"));
        logName.sendKeys("username");

        WebElement logPassw=driver.findElement(By.xpath("//*[@id='password']"));
        logPassw.sendKeys("password");

        WebElement logIn=driver.findElement(By.xpath("//*[@class='login-submit']"));
        logIn.click();

        WebElement logButton= (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='gwt-uid-8']")));

        logButton.click();

        WebElement logOut=(new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Log out']")));
        logOut.click();

        WebElement logoElem = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='login-logo']")));

        boolean isDisp=logoElem.isDisplayed();

        Assert.assertTrue(isDisp, "Logo Axiom is displayed");

        driver.close();

}
}
