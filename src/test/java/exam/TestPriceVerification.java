package exam;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestPriceVerification {
    @Test
    public void priceVerification() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://skyup.aero/ru/");
        WebElement cookieButton = driver.findElement(By.xpath("//*[@class='cookie-modal__btn btn-7 js-cookie-modal-accept']"));
        cookieButton.click();

        WebElement deprturCityCityName = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='deprtureCity']//*[@class='form__city-block']")));
        deprturCityCityName.click();

        WebElement departCitySearch = driver.findElement(By.xpath("//*[@id='citiesSearch']"));
        departCitySearch.click();
        departCitySearch.sendKeys("HRK");
        departCitySearch.sendKeys(Keys.ENTER);

        WebElement arrivCityName = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@class,'form__item form__item--empty')]//*[@class='form__city-placeholder-text']")));
        arrivCityName.click();

        WebElement arrivCitySearch = driver.findElement(By.xpath("//*[@id='citiesSearch']"));
        arrivCitySearch.sendKeys("LWO");
        arrivCitySearch.sendKeys(Keys.ENTER);

        WebElement dateDeparture =(new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='forwardDateItem']//*[@class='form__item-date']")));
        dateDeparture.click();

        WebElement month1 = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='next']")));
        Thread.sleep(2000);
        month1.click();

        WebElement month2 = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='next']")));
        Thread.sleep(2000);
        month2.click();


       WebElement month3 = driver.findElement(By.xpath("//*[@class='next']"));
        Thread.sleep(2000);
      month3.click();

       // WebElement findDate = driver.findElement(By.xpath("//*[@time='1583964000000']"));

        WebElement priceForDate = driver.findElement(By.xpath("//*[@time='1583964000000'] //*[@data-price='812']"));
        String text=priceForDate.getText();
        String [] a=text.split("\\W+");
        int priceValue = Integer. parseInt(a[0]);


        Assert.assertTrue(priceValue<2000);

        driver.close();











    }
}
