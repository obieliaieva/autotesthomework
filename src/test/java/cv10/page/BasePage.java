package cv10.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

import java.util.concurrent.TimeUnit;

public class BasePage {

    public WebDriver driver;
    public WebDriver getDriver(){
        driver=new ChromeDriver();
        return driver;
    }
    public BasePage open(String url){
        getDriver().get(url);
        return this;
    }

    @AfterMethod
    public void close(){
        driver.quit();
    }
}
