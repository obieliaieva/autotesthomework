package cv10.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class LogInPage extends Header {
    String url = "https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index";
    By logName= By.xpath("//*[@id='username']");
    By logPassw = By.xpath("//*[@id='password']");
    By logIn = By.xpath("//*[@class='login-submit']");
    public By project = By.xpath("//*[text()='RBI_015']");

    public LogInPage logIn(String usnm, String psw) {
        open(url);
        driver.findElement(logName).sendKeys(usnm);
        driver.findElement(logPassw).sendKeys(psw);
        driver.findElement(logIn).click();
        driver.manage().window().maximize();
        return this;


    }

}

