package cv10.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Projects extends LogInPage {
    By findAllBranches=By.xpath("//*[@id='ALL_BRANCHES']");

    By lookupProject=By.xpath("//*[contains(@class,'split-pane-component-1')]//*[@id='ic-lookup-big']");
    //By searchProject=By.xpath("//input");

    By inputProjectName=By.xpath("//input[@type='text']");

    public WebDriver searchForProject(String project){
        driver.findElement(findAllBranches).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(lookupProject).click();
        driver.findElement(inputProjectName).sendKeys(project);
        return driver;
    }
}
