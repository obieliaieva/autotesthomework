package cv10.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Header extends BasePage {
    public By logInButton=By.xpath("//*[@id='gwt-uid-8']");
    public By logOutButton=By.xpath("//*[text()='Log out']");
    public By menuBurger=By.xpath("//*[@id='gwt-uid-3']");
    public By sysSettings=By.xpath("//*[text()='System Settings']");
    public By dbSourceButton=By.xpath("//*[text()='DB Source']");

    public WebDriver goToDBsource(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(menuBurger).click();
        driver.findElement(sysSettings).click();
        driver.findElement(dbSourceButton).click();
        return driver;
    }
    public WebDriver logOut(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(logInButton).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(logOutButton).click();
        return driver;
    }
}
